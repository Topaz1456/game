import random

class Car:
    def __init__(self):
        self.__speed = 30
        self.__odometer = 0
        self.__time = 0
        self.__hit_points = 5
        self.__tire_status = "NORMAL"
        self.__tire_status_counter = 0

    def set_tire_status(self, condition):
        self.__tire_status = condition

    def set_tire_status_counter(self, count):
        self.__tire_status_counter += count

    def get_speed(self):
        return self.__speed

    def get_odometer(self):
        return self.__odometer

    def get_hit_points(self):
        return self.__hit_points

    def get_tire_status(self):
        return self.__tire_status

    def get_tire_status_counter(self):
        return self.__tire_status_counter

    def accelerate(self):
        self.__speed += random.randint(1, 5)

    def brake(self):
        if self.__speed > 20:
            self.__speed -= random.randint(1, 5)
        else:
            print("Not possible.")

    def step(self):
        self.__odometer += self.__speed
        self.__time += 1

    def average_speed(self):
        return self.__odometer / self.__time

    def receive_damage(self):
        self.__hit_points -= 1

    def recover_damage(self):
        if self.__hit_points < 5:
            self.__hit_points += 1

    def instant_death(self):
        self.__hit_points = 0

    def reset_tire_status(self):
        self.__tire_status = "NORMAL"

    def tire_status_turn(self):
        if self.__tire_status_counter > 0:
            self.__tire_status_counter -= 1
