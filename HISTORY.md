#History Log Updates (GAME)

#1) Created basic structure as well as an MVC model architecture for the game
#2) Slight modification in def test of class Situations. Set default and minimum speed to 20
#3) Added sub-scenarios. Added tire status, and set the default speed to 30. Now includes a game over condition.
#4) Added minor detail in one of the situations.
#5) Added a new function for the tire status.
#6) Minor modification on one of the functions in class Car
#7) Added a run.py file