from model import Car, Situations
from view import Display

def run():

    if __name__ == '__main__':
        intro_flag = False
        my_car = Car.Car()

        if not intro_flag:
            print("Driving simulation start!")

        while True:
            if my_car.get_hit_points() == 0:
                print("You died.")
                break

            print("Speed =", my_car.get_speed())
            print("HP =", my_car.get_hit_points())
            print("Tire Status=", my_car.get_tire_status())
            action = Situations.Situation.select_action()

            if action not in "ABOS" or len(action) != 1:
                print("Huh?")
            if action == 'A':
                my_car.accelerate()
            elif action == 'B':
                my_car.brake()
            elif action == 'O':
                Display.Print.odometer_stats(my_car)
            elif action == 'S':
                Display.Print.average_speed_status(my_car)
            my_car.step()
            Display.Print.say_state(my_car)
            Situations.Situation.test(my_car)
