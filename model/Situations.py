import random

class Situation:
    @staticmethod
    def test(car):
        situations = {1: "You are approaching the intersection that's 30m ahead.",
                      2: "You are approaching a tight curve.",
                      3: "You encountered heavy traffic. What will you do?",
                      4: "It's raining.",
                      5: "The road's slippery."}

        choice = random.randint(1, 5)

        print(situations[choice])

        if choice == 1:
            if car.get_speed() >= 30:
                print("You succeeded!")
            else:
                print("You were too slow and you collided with a truck loaded with explosives.")
                car.receive_damage()

        elif choice == 2:
            if car.get_speed() <= 35:
                print("You made a smooth turn.")
            else:
                print("Your speed is too fast to make a turn. You drive straight through.")
                car.receive_damage()

                if car.get_tire_status() == "WET":
                    car.receive_damage()

        elif choice == 3:
            action = input("[D]rink a can of beer to recuperate, [S]nooze off until the traffic jam lightens up, Be [a]ggressive and sound your horn continously > ").upper()

            if action == 'D':
                print("You decided to chug down some mediocre quality beer. You weren't satisfied.")

            elif action == "S":
                dreams = {1: "Fate Grand Order",
                          2: "Senran Kagura",
                          3: "Kingdom Hearts",
                          4: "Kingdom of Make-Believe",
                          5: "Super Smash Bros.",
                          6: "The Merc with the Mouth"}
                satisfaction = {1: "satisfied",
                                2: "unsatisfied"}
                choice2 = random.randint(1, 6)
                choice3 = random.randint(1, 2)

                print("You dozed off to the world of", dreams[choice2])
                print("You woke up feeling", satisfaction[choice3])

                if choice3 == 1:
                    car.recover_damage()

            elif action == "A":
                disasters = {1: "there was a stowaway at the trunk of your car and peppered spray you for inexplicable reasons.",
                             2: "the cement truck mixer falls on top of you, crushing you beneath."}
                choice2 = random.randint(0, 2)

                print("You were so impatient, you fail to realize that", disasters[choice2])

                if choice2 == 1:
                    car.receive_damage()
                elif choice2:
                    car.instant_death()

        elif choice == 4:
            car.set_tire_status("WET")

        elif choice == 5:
            if car.get_tire_status() == "WET":
                print("Your tire is insanely wet as your car skids out of control.")
                car.receive_damage()
                car.reset_tire_status()

            elif car.get_tire_status() == "NORMAL":
                print("You drive carefully, not skidding out of control. Your tire became wet afterwards.")
                car.set_tire_status("WET")
