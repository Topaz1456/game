import sys

sys.path.append('model')
sys.path.append('controller')
sys.path.append('view')

from controller.AccessCar import run

if __name__ == "__main__":
    run()