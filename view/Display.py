class Print:
    @staticmethod
    def say_state(car):
        print("I'm going {} kph!".format(car.get_speed()))

    @staticmethod
    def odometer_stats(car):
        print("The car has driven {} kilometers".format(car.get_odometer()))

    @staticmethod
    def average_speed_status(car):
        print("The car's average speed was {} kph".format(car.average_speed()))

